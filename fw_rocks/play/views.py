from django.http import HttpResponse

from .views_package.logic import (
    MOVE_LIZARD,
    MOVE_PAPER,
    MOVE_ROCK,
    MOVE_SCISSORS,
    MOVE_SPOCK,
    play_action_logic,
)

from .models import Game, Round, User
from .views_package.creation import create_complete_game

# Utility functions
def play_action(request, game_id: int, nickname: str, action: str):
    http_response_txt = play_action_logic(game_id, nickname, action)
    return HttpResponse(http_response_txt)


# Create your views here.
def index(request):
    return HttpResponse("Hello, world. You're at the play index.")


def create_game(request, nickname1: str, nickname2: str, description: str):
    game, round = create_complete_game(
        nickname1=nickname1,
        nickname2=nickname2,
        description=description,
    )
    return HttpResponse(
        f"You are creating a new game (ID={game.pk}) between players {nickname1} and {nickname2}."
    )


def forfeit(request, game_id: int, nickname: str):
    """Will make other player score if he/she played already (is it the rule?), or at least terminate round.
    And will not create another round."""
    return HttpResponse(f"MUST IMPLEMENT FORFEIT")


def play_rock(request, game_id: int, nickname: str):
    return play_action(request, game_id, nickname, MOVE_ROCK)


def play_scissor(request, game_id: int, nickname: str):
    return play_action(request, game_id, nickname, MOVE_SCISSORS)


def play_paper(request, game_id: int, nickname: str):
    return play_action(request, game_id, nickname, MOVE_PAPER)


def play_lizard(request, game_id: int, nickname: str):
    return play_action(request, game_id, nickname, MOVE_LIZARD)


def play_spock(request, game_id: int, nickname: str):
    return play_action(request, game_id, nickname, MOVE_SPOCK)


def list_top_scores(request):
    user_scores = User.objects.order_by("-score")[:100]
    display = "TOP SCORES\n"
    display += "Nickname\tScore\n"
    display += "\n".join(
        [user.nickname.ljust(10) + "\t" + str(user.score) for user in user_scores]
    )
    return HttpResponse(f"{display}", content_type="text/plain")


def get_nickname_from_user_pk(user_pk):
    return User.objects.get(pk=user_pk).nickname


def list_games_for_user(request, nickname):
    games = Game.objects.filter(users__nickname__contains=nickname)
    display = f"Games for user {nickname}\n"
    display += "\n".join(
        [
            str(game.pk)
            + "\t"
            + "\t".join([user.nickname for user in game.users.all()])
            + "\t"
            + game.description.ljust(40)
            + "\t"
            + ("finished" if game.is_finished else "running")
            for game in list(games)
        ]
    )
    return HttpResponse(f"{display}", content_type="text/plain")


def list_rounds_for_game(request, game_id):
    rounds = Round.objects.filter(game_id=game_id)
    display = f"Rounds for game {game_id}\n"
    display += "\n".join([str(round) for round in list(rounds)])
    return HttpResponse(f"{display}", content_type="text/plain")
