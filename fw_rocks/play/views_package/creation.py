from django.http import HttpResponse

from ..models import Move, User, Game, Round


def create_round(game: Game):
    # Create round and save it
    round = Round(game_id=game)
    round.save()
    # Change game state and change it
    game.is_finished = False
    game.save()
    return round


def create_complete_game(nickname1: str, nickname2: str, description: str):
    if nickname1 == nickname2:
        return HttpResponse(
            f"ERROR, NOT creating a new game between identical players {nickname1} and {nickname2}."
        )
    user_1 = User.objects.get(
        nickname=nickname1
    )  # niark will fault if user1 does not exist
    user_2 = User.objects.get(
        nickname=nickname2
    )  # niark will fault if user2 does not exist
    game = Game(description=description)
    game.save()
    game.users.add(user_1)
    game.users.add(user_2)
    game.save()
    round, create_round(game)
    return game, round


def record_move(round: Round, action: str, user):
    # I feel the Move design is wrong... but will do the trick for the PoC
    # Maybe basic moves could be registered, and then some other object/association could exist
    # Or use another ManyToMany in Round for users, and make moves+users ordering coherent
    # For now, it helps coherence for who played what for a round, and overall cost is
    # nb_users x nb_possible_moves... Room for improvement!
    try:
        # Has user already played this move?
        move = Move.objects.filter(move_name=action, user_id=user).last()
        # Previously I recorded multiple identical move per user, hence the last
        if move is None:
            raise Move.DoesNotExist
    except Move.DoesNotExist:
        # Nope, create it
        move = Move(move_name=action, user_id=user)  # niark error if action unkonwn
        move.save()
    round.moves.add(move)
    round.save()
    return move
