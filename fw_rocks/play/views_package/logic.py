"""logic is too big of a file, should be split."""

from dataclasses import dataclass
from typing import Final, List, Tuple

from ..models import Game, Move, Round, User
from .creation import create_round, record_move

USER_WINNER_1: Final[str] = "user"
USER_WINNER_2: Final[str] = "other"
USER_WINNER_DRAW: Final[str] = "draw"

MOVE_ROCK: Final[str] = "rock"
MOVE_PAPER: Final[str] = "paper"
MOVE_SCISSORS: Final[str] = "scissors"
MOVE_SPOCK: Final[str] = "spock"
MOVE_LIZARD: Final[str] = "lizard"


@dataclass
class GenericMove:
    move_name: str
    i_beat_move: Tuple[str]
    i_am_beaten_by_move: Tuple[str]

    def fight_other_move(self, other_move_name: str) -> str:
        if other_move_name in self.i_am_beaten_by_move:
            return USER_WINNER_2
        if other_move_name in self.i_beat_move:
            return USER_WINNER_1
        return USER_WINNER_DRAW


move_rock = GenericMove(
    move_name=MOVE_ROCK,
    i_beat_move=(MOVE_SCISSORS, MOVE_LIZARD),
    i_am_beaten_by_move=(MOVE_PAPER, MOVE_SPOCK),
)

move_paper = GenericMove(
    move_name=MOVE_PAPER,
    i_beat_move=(MOVE_ROCK, MOVE_SPOCK),
    i_am_beaten_by_move=(MOVE_SCISSORS, MOVE_LIZARD),
)

move_scissors = GenericMove(
    move_name=MOVE_SCISSORS,
    i_beat_move=(MOVE_PAPER, MOVE_LIZARD),
    i_am_beaten_by_move=(MOVE_ROCK, MOVE_SPOCK),
)

move_lizard = GenericMove(
    move_name=MOVE_LIZARD,
    i_beat_move=(MOVE_SPOCK, MOVE_PAPER),
    i_am_beaten_by_move=(MOVE_ROCK, MOVE_SCISSORS),
)

move_spock = GenericMove(
    move_name=MOVE_SPOCK,
    i_beat_move=(MOVE_SCISSORS, MOVE_ROCK),
    i_am_beaten_by_move=(MOVE_LIZARD, MOVE_PAPER),
)

move_association = {
    MOVE_SPOCK: move_spock,
    MOVE_LIZARD: move_lizard,
    MOVE_PAPER: move_paper,
    MOVE_ROCK: move_rock,
    MOVE_SCISSORS: move_scissors,
}


def get_winner_move(
    move_name: str, user1: User, other_move_name: str, user2: User
) -> str:
    if other_move_name not in move_association:
        raise ValueError(
            f"Move {other_move_name} not in authorized moves list {move_association.keys()}"
        )
    # dict will throw a KeyError if move_name not registered
    fight_result = move_association[move_name].fight_other_move(other_move_name)
    if fight_result == USER_WINNER_1:
        return user1
    elif fight_result == USER_WINNER_2:
        return user2
    else:
        return None


def get_move_info_from_round(round: Round, user_index: int):
    """beh should it be here?"""
    return (
        round.moves.all()[user_index].move_name,
        round.moves.all()[user_index].user_id,
    )


def increment_score_user(user: User):
    user.score += 1
    user.save()


def score_game(round):
    move_name, user = get_move_info_from_round(round, 0)
    other_move_name, other_user = get_move_info_from_round(round, 1)
    winner_user = get_winner_move(move_name, user, other_move_name, other_user)
    if winner_user:
        increment_score_user(winner_user)
        return winner_user


def finalize_round_for_game(game: Game, round: Round) -> User:
    winner_user: User = score_game(round)
    game.is_finished = True
    game.save()
    return winner_user


def play_action_logic(game_id: int, nickname: str, action: str):
    # Question: what about concurrent moves????
    game: Game = Game.objects.get(pk=game_id)  # niark will fault if game does not exist
    user: User = User.objects.get(
        nickname=nickname
    )  # niark will fault if user does not exist
    if user not in game.users.all():
        # user should not play this round!
        return f"User {user.nickname} unauthorized in this game."
    if game.is_finished:
        # let's go for another round, meaning that even if previously there was forfeit
        # We can keep on playing transparently
        round: Round = create_round(game)
    else:
        # retrieve current round
        round: Round = Round.objects.filter(
            game_id=game_id
        ).last()  # last round by default (pk ordering)
        # Verify already played?
        if len(round.moves.all()) == 1:  # comparison to one useless, >0 sufficient
            # Only one player, is it current user?
            if user == round.moves.all()[0].user_id:
                # Can't play twice for current round!
                return f"Movement unauthorized: you already played {round.moves.all()[0].move_name}."

    # OK Record move
    move: Move = record_move(round, action, user)
    # everyone already played? Should rather check if both authorized users are in round.moves
    if len(round.moves.all()) == 2:
        winner_user: User = finalize_round_for_game(game=game, round=round)
        # Settle the score
        return f"Finished round {round.pk} for game {game.pk}: {winner_user.nickname if winner_user else 'None'} won, users played {' against '.join([ m_move.move_name for m_move in round.moves.all()])} "
    # Ok, first player played
    return f"Recorded move {move.move_name} for user {nickname} in game {game.pk}, round {round.pk}"
