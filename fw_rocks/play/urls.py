from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('create/<str:nickname1>/<str:nickname2>/<str:description>', views.create_game, name='create_game'),
    path('move/<int:game_id>/<str:nickname>/rock', views.play_rock, name='play_rock'),
    path('move/<int:game_id>/<str:nickname>/paper', views.play_paper, name='play_paper'),
    path('move/<int:game_id>/<str:nickname>/scissor', views.play_scissor, name='play_scissor'),
    path('move/<int:game_id>/<str:nickname>/spock', views.play_spock, name='play_spock'),
    path('move/<int:game_id>/<str:nickname>/lizard', views.play_lizard, name='play_lizard'),
    path('forfeit/<int:game_id>/<str:nickname>', views.forfeit, name='forfeit'),
    path('list/games/<str:nickname>', views.list_games_for_user, name='list_games_for_user'),
    path('list/rounds/<int:game_id>', views.list_rounds_for_game, name='list_rounds_for_game'),
    path('list/scores/top', views.list_top_scores, name='list_top_scores'),
]