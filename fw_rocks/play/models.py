from django.db import models

# Create your models here.

class User(models.Model):
    """Describe a user, we put score here for perf. sake.
    Could be computed on the fly and not stored"""
    nickname = models.CharField(max_length=200)
    score = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.pk}, {self.nickname}, {self.score}"

class Game(models.Model):
    """Describe a game: two users will play"""
    users = models.ManyToManyField(User)
    description = models.CharField(default="No Description", max_length=200)
    is_finished = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.pk}, {self.users}, {self.description}, {self.is_finished}"

class Move(models.Model):
    """Describe one move action for the game"""
    move_name = models.CharField(max_length=20)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.pk}, {self.move_name}, {self.user_id}"

class Round(models.Model):
    """Describe one round of a game.
    Moves are associated to users by following Game user ordering.
    .ie move1 == user1, move2 == user2"""
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
    moves = models.ManyToManyField(Move)
    users = models.ManyToManyField(User)

    def __str__(self):
        return f"{self.pk}, {self.game_id}, {self.moves}"




