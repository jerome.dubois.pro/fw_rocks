from django.contrib import admin

# Register your models here.

from .models import Game, User, Move, Round

admin.site.register(Game)
admin.site.register(User)
admin.site.register(Move)
admin.site.register(Round)
