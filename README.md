# Install FW_Rocks
```bash
git clone git@gitlab.com:jerome.dubois.pro/fw_rocks.git
python3 -mvenv venvs/prod
source venvs/prod/bin/activate
pip install -U pip
pip install django
cd fw_rocks/fw_rocks/

# Run the PoC
python3 manage.py runserver
```
# Open another terminal
```bash
# Let's play a litle bit on an existing game
for i in Hello1,lizard Hello2,rock Hello1,paper Hello2,paper Hello1,rock Hello2,spock ; do nickname=$(echo $i | cut -d',' -f1); move_name=$(echo $i | cut -d',' -f2); curl http://127.0.0.1:8000/play/move/3/${nickname}/${move_name};echo;echo; done
# list games for a user
curl http://127.0.0.1:8000/play/list/games/Hello1
echo
# list rounds for a game
curl http://127.0.0.1:8000/play/list/rounds/3
echo
# list top scores
curl http://127.0.0.1:8000/play/list/scores/top
echo
# create a new game (should give PK/ID=4)
curl http://127.0.0.1:8000/play/create/Hello3/Hello2/Test_FW_description
echo
# play it
curl http://127.0.0.1:8000/play/move/4/Hello1/lizard # Unauthorized user (could be a token and real user retrieved from DB)
echo
curl http://127.0.0.1:8000/play/move/4/Hello2/lizard
echo
curl http://127.0.0.1:8000/play/move/4/Hello3/rock
echo
```
# Notes
The way I handle Move/User association for a round is not gracious at all, and I think design is flawed: need to record TypeOfMovexUser once in the Move table. So, 1000 users and 5 moves => 5000 records... This is a bad design, and my only excuse to do this is to get a convenient moveXuser association for each move in a round: (move1,user1), (move2,user2). Originally, I would store every move for each user, therefore there are redundant records in DB for say (move1,user1), (move1,user1). That is why at some point when retrieving moves for a user, I use .last() to get only one... should not happen anymore, but well... it could have side effect in the current state of the code.

To be noted, I am so ashamed not to provide pytest, mypy+flake8+pylint CI/CD validation, for the (wrong reason!) sake of time.

If I had time, I would first unit test the views_package folder as it does not depend on django, it is the core of the application logic. Then think about django testing (unfamiliar), maybe through mocking... Need to see what the best practices are...
